\newcommand{\lecturenumber}{3}
\input{./intro/intro.tex}

\begin{document}
	\maketitle

\begin{defn}
	Let $X$ be a topological space.
	\begin{enumerate}
		\item Define the category $\Ouv{X}$ to be the category with open sets $U \subseteq X$ as objects and inclusions of such open sets as morphisms.
		\item A \emph{presheaf} of sets, resp.\ (abelian) groups, resp.\ rings on $X$ is a functor 
		\[\calF \colon \opcat{\Ouv{X}} \to \calC, \]
		where $\calC = \cat{Set}, \cat{Grp}, \cat{Ab}, \cat{Rings}$ respectively. More concretely, a presheaf $\calF$ on $X$ assigns an object $\calF(U) \in \calC$ to every open subset $U \subseteq X$ and a map $\calF(U) \to \calF(V)$, denoted by $s \mapsto \restr{s}{V}$, to every inclusion $V \subseteq U$.
		\item Let $\calF$ be a presheaf (of some kind) on $X$.
		We call $\calF$ a \emph{sheaf} if for every open cover $U = \bigcup_{i} U_i$ of every open subset $U \subseteq X$ the map
		\[ \calF(U) \to \set*{(s_i)_i \in \prod_i \calF(U_i)}{\restr{s_i}{U_i \cap U_j} = \restr{s_j}{U_i \cap U_j} \text{ for all $i, j$}} \]
		given by $s \mapsto (\restr{s}{U_i})_i$ is bijective.
	\end{enumerate}
	We denote by $\Psh{\calC}{X}$ the category of $\calC$-valued presheaves on $X$.
	Morphisms in this category are given by natural transformations between functors.
	We also denote by $\Sh{\calC}{X}$ the full subcategory of $\Psh{\calC}{X}$ spanned by sheaves.
\end{defn}

\begin{defn}
	Let $X$ be a topological space and let $\calF$ be a sheaf (of something) on $X$.
	For a point $x \in X$, we define the \emph{stalk} of $\calF$ at $x$ to be
	\[ \calF_x \coloneqq \varinjlim_U \calF(U), \]
	where the (filtered) colimit is taken over the set of open subsets $U \subseteq X$ containing $x$.
\end{defn}

\problemsstart

\begin{exercise}
	Let $X$ be a topological space.
	\begin{enumerate}[label = \alph*)]
		\item Show that the assignment
		\[U \longmapsto \set*{f \colon U \rightarrow \RR}{f \text{ is continuous}} \]
		defines a sheaf (of rings) on $X$.
		\item Show that the assignment
		\[ U \longmapsto \set*{f \colon U \rightarrow \RR}{f \text{ is bounded}} \]
		defines a presheaf (of rings) on $X$ that may fail to be a sheaf.
	\end{enumerate}
\end{exercise}

\begin{exercise}
	Let $X$ be a topological space.
	Let $\calC$ be either $\cat{Set}$, $\cat{Grp}$, $\cat{Ab}$ or $\cat{Rings}$.
	Show that limits in the category $\Sh{\calC}{X}$ can be computed pointwise. 
	More precisely, show that for any diagram $i \mapsto \calF_i$ in the category $\Sh{\calC}{X}$ and any open subset $U \subseteq X$ we have
	\[ \left( \varprojlim_i \calF_i \right) (U) \cong \varprojlim_i \calF_i(U). \]
	Is the same thing true for colimits?
\end{exercise}

\begin{exercise}
	Let $X$ be a topological space.
	Let $\varphi \colon \calF \to \calG$ be a map of sheaves of sets, resp.\ abelian groups on $X$.
	\begin{enumerate}[label = \alph*)]
		\item Show that $\varphi$ is mono iff for every open subset $U \subseteq X$ the map $\varphi_U \colon \calF(U) \to \calG(U)$ is injective.
		\item Show that $\varphi$ is epi iff for every open subset $U \subseteq X$ and every $t \in \calG(U)$ there exists an open cover $U = \bigcup_i U_i$ and elements $s_i \in \calF(U_i)$ such that $\varphi_{U_i}(s_i) = \restr{t}{U_i}$ for all $i$.
	\end{enumerate}
	Also check that the injective (resp.\ surjective) maps of sheaves (as defined in the lecture using stalks) are exactly the monos (resp.\ epis).
\end{exercise}

\begin{exercise}
	Let $X$ be a topological space.
	Let $\calF$ be a presheaf (of something) on $X$.
	\begin{enumerate}[label = \alph*)]
		\item Suppose we are given subobjects $\calF'(U) \subseteq \calF(U)$ for every $U \subseteq X$ and suppose that for every inclusion $V \subseteq U$ and every $s \in \calF'(U)$ we have $\restr{s}{V} \in \calF'(V)$.
		Show that this data then defines a presheaf $\calF'$.
		A presheaf of this form is called a \emph{subpresheaf} of $\calF$.
		\item Suppose that $\calF$ is a sheaf. Show that it is not generally true, that every subpresheaf $\calF' \subseteq \calF$ is automatically a sheaf.
		Can you give a nice characterization of those subpresheaves that are sheaves?
		These are called subsheaves.
	\end{enumerate}
\end{exercise}

\begin{exercise}
	Let $X = \set*{1/n}{n \in \NN} \cup \{ 0 \} \subseteq \RR$ and let $A$ be a set.
	For each open subset $U \subseteq X$, define
	\[ \calF(U) \coloneqq \left\{ f \colon U \setminus \{0\} \to A \right\}. \]
	This gives rise to a sheaf of sets $\calF$ on $X$ (if this is not clear to you, check it).
	Show that the stalk $\calF_0$ can be identified with the set of sequences in $A$, indexed over the natural numbers, where two such sequences $(a_n)_n$ and $(b_n)_n$ are considered equal if there exists $N \in \NN$ with $a_n = b_n$ for all $n \geq N$.
\end{exercise}

\end{document}