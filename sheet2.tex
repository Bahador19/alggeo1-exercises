\newcommand{\lecturenumber}{2}
\input{./intro/intro.tex}

\begin{document}
	\maketitle

\begin{defn}
	A topological space $X$ is called \emph{Noetherian} if it satisfies the descending chain condition for closed subsets.
	More precisely this means that for all chains
	\[ A_1 \supseteq A_2 \supseteq \dots \]
	of closed subsets of $X$, there exists $N \geq 1$ such that $A_n = A_N$ for all $n \geq N$.
\end{defn}

\begin{defn}
	Let $X$ be a topological space and let $Z \subseteq X$.
	\begin{enumerate}
		\item $Z$ is called \emph{irreducible} if it is nonempty and cannot be written non-trivially as a union of two relatively closed subsets $A, B \subseteq Z$.
		\item $Z$ is called an irreducible component of $X$ if it is maximal with the property of being irreducible.
	\end{enumerate}
\end{defn}

\begin{defn}
	A topological space $X$ is called \emph{sober} if every irreducible closed subset $Z \subseteq X$ has a unique generic point $\eta_Z$ (this means $\clos{\{\eta_Z\}} = Z$).
\end{defn}

\problemsstart

\begin{exercise}
	Let $R$ be a ring.
	Let $A, B$ be $R$-algebras.
	\begin{enumerate}[label = \alph*)]
		\item Show that $A \tens{R} B$ has the structure of an $R$-algebra with multiplication given on pure tensors by 
		\[(a \smtens b) (a' \smtens b') = (aa' \smtens bb'). \]
		\item Show that the maps
		\[ A \to A \tens{R} B, \quad a \mapsto a \smtens 1 \]
		and
		\[ B \to A \tens{R} B, \quad b \mapsto 1 \smtens b \]
		are maps of $R$-algebras.
		Moreover show that these maps identify $A \tens{R} B$ as a pushout of $A$ and $B$ over $R$.
	\end{enumerate}
\end{exercise}

\begin{exercise}
	Let $K, K', K''$ be fields and suppose we are given maps of fields $K \to K'$ and $K \to K''$.
	Show that there exists a field $\Omega$ and a commutative diagram of the following form:
	\[
	\begin{tikzcd}[row sep = small]
	&
	K' \ar{rd}&
	\\
	K \ar{ru} \ar{rd}&
	&
	\Omega \\
	&
	K'' \ar{ru}
	&
	\end{tikzcd}
	\]
\end{exercise}

\begin{exercise}
	Let $I$ be a filtered partially ordered set.
	Let $(A_i)_i$ be an inductive system of abelian groups (resp. rings) indexed over $I$.
	Show that the colimit $\varinjlim_i A_i$ can be calculated on the underlying sets of the $A_i$'s.
	More precisely, show that $\varinjlim_i A_i$ is given by
	\[ \varinjlim_i A_i = \left. \bigsqcup_i A_i \middle\ovr {\sim} \right. . \]
	Here we identify $x_i \in A_i$ and $x_j \in A_j$ iff they map to the same element of $A_k$ for some $k \geq i, j$.
	It is part of the exercise to define the correct abelian group resp.\ ring structure on this set.
\end{exercise}

\begin{exercise}
	Let $R$ be a ring.
	Show that $\Spec{R}$ is a Noetherian topological space if $R$ is Noetherian.
	Is the converse statement true?
\end{exercise}

\begin{exercise}
	Let $X$ be a topological space.
	\begin{enumerate}[label = \alph*)]
		\item Let $Z \subseteq X$ irreducible and let $U \subseteq Z$ be a nonempty open subset.
		Show that $U$ is irreducible.
		\item Show that every irreducible subset $Z \subseteq X$ is contained in an irreducible component of $X$.
		\item Show that the irreducible components of $X$ are closed.
	\end{enumerate}
\end{exercise}

\begin{exercise}
	Let $X$ be a Noetherian topological space.
	Show that $X$ has only finitely many irreducible components.
	Deduce that a Noetherian ring $R$ has only finitely many minimal prime ideals.
\end{exercise}

\begin{exercise}
	Let $X$ be a topological space.
	Show that there is a sober topological space $\sober{X}$ together with a map $X \to \sober{X}$ satisfying the following universal property:
	
	For every sober topological space $T$ and every map $X \to T$, there is a unique map $\sober{X} \to T$ such that the following diagram commutes:
	\[
	\begin{tikzcd}
	X \ar{r} \ar{d}&
	T\\
	\sober{X} \ar{ru}&
	\end{tikzcd}
	\]
\end{exercise}

\begin{exercise}
	Let $k$ be an algebraically closed field.
	Let $X \subseteq \affsp{n}(k)$ be an affine algebraic set with coordinate ring $R \coloneqq \calO(X)$.
	Show that we have an identification $\Spec{R} \cong \sober{X}$.
	
	Is it (more generally) true that we have $\Spec{R} \cong \sober{\MaxSpec{R}}$ for an arbitrary ring $R$?
\end{exercise}

\end{document}