\newcommand{\lecturenumber}{1}
\input{./intro/intro.tex}

\begin{document}
	\maketitle

\begin{nonumconvention}
	Rings are unital and commutative.
	Ring maps respect units.
\end{nonumconvention}

\begin{lem}[5-Lemma]
	Let $R$ be a ring.
	Suppose we are given the following commutative diagram of $R$-modules with exact rows:
	\[
	\begin{tikzcd}
	A \ar{r} \ar{d}[right]{\alpha}&
	B \ar{r} \ar{d}[right]{\beta}&
	C \ar{r} \ar{d}[right]{\gamma}&
	D \ar{r} \ar{d}[right]{\delta}&
	E \ar{d}[right]{\varepsilon} \\
	A' \ar{r}&
	B' \ar{r}&
	C' \ar{r}&
	D' \ar{r}&
	E'
	\end{tikzcd}
	\]
	Also suppose that $\alpha$ is surjective, $\varepsilon$ is injective and $\beta, \delta$ are bijective.
	Then $\gamma$ is also bijective.
\end{lem}

\begin{defn}
	Let $R$ be a ring and let $I \subseteq R$ be an ideal.
	Define the \emph{completion} $\complete{R}{I}$ as
	
	\[ \complete{R}{I} \coloneqq \varprojlim R/I^n
	= \set*{(x_n)_n \in \prod_n R/I^n}{x_n \equiv x_{n+1} \Mod{I^n} \text{ for all $n$}}. \]
	
	This is a ring with component-wise operations and in fact an $R$-algebra with the obvious map $R \to \complete{R}{I}$.
\end{defn}

\begin{defn}
	Let $X$ be a topological space.
	\begin{enumerate}
		\item We say that $X$ is \emph{quasi-compact} if for every open cover $X = \bigcup_{i \in I} U_i$ there is a finite subset $J \subseteq I$ such that $X = \bigcup_{i \in J} U_i$.
		\item We say that $X$ is \emph{$T_0$} if for every two distinct points $x, y \in X$ there exists an open subset $U \subseteq X$ containing exactly one of them.
	\end{enumerate}
\end{defn}

\problemsstart

\begin{exercise}
	Let $K$ be a field. Show that the following are equivalent:
	\begin{enumerate}
		\item $K$ is algebraically closed.
		\item For every finite extension of fields $L \ovr K$, we already have $L = K$.
	\end{enumerate}
\end{exercise}

\begin{exercise}
	Describe the maximal ideals of the following rings:
	\[ \adjoin{\CC}{x}, \quad
	\adjoin{\RR}{x}, \quad
	\adjoin{\CC}{x, y}, \quad
	( \text{bonus: } \adjoin{\RR}{x, y} ). \]
	Can you draw pictures?
\end{exercise}

\begin{exercise}
	Let $R$ be a ring.
	Let $\ses{M'}{M}{M''}$ be a short exact sequence of $R$-modules.
	Show that $M$ is Noetherian if and only if $M'$ and $M''$ are both Noetherian.
\end{exercise}

\begin{exercise}
	Let $k$ be a field.
	Let $f(x, y) \coloneqq y^2-x^2(x+1) \in \adjoin{k}{x, y}$.
	Show that $f$ is irreducible.
\end{exercise}

\begin{exercise}
	Let $R \subseteq S$ be an integral extension of rings.
	Show that we have the equality $\units{R} = R \cap \units{S}$.
\end{exercise}

\begin{exercise}
	Let $k$ be a field.
	Let $R \coloneqq \adjoin{k}{x, y}/(xy-1)$.
	Find an element $z \in R$ such that $R$ is finite over $\adjoin{k}{z}$.
\end{exercise}

\begin{exercise}
	Let $R$ be a ring.
	Let $S$ be an $R$-algebra.
	Let $M, N$ be $R$-modules.
	Show that there is a natural map
	\[ \alpha \colon S \tens{R} \Hom{R}{M}{N} \longrightarrow \Hom{S}{S \tens{R} M}{S \tens{R} N} \]
	of $S$-modules, given on pure tensors by $s \smtens f \mapsto s \cdot (\id{S} \smtens f)$.
	Also show that $\alpha$ is an isomorphism, provided that $S$ is flat over $R$ and that $M$ is finitely presented.
	
	(Hint: First assume that $M$ is finite free and then use the 5-Lemma.)
\end{exercise}

\begin{exercise}
	Let $R$ be a ring.
	Let $\ses{M'}{M}{M''}$ be a short exact sequence of $R$-modules.
	Assume that $M''$ is finitely presented and that for all maximal ideals $\idealm \subseteq R$ the short exact sequence
	\[ \ses{M'_{\idealm}}{M_{\idealm}}{M''_{\idealm}} \]
	of $R_{\idealm}$-modules is split.
	Show that also the original sequence is split.
	
	(Hint: Use the previous exercise and the fact that a short exact sequence $\ses{M'}{M}{M''}$ of $R$-modules is split iff the map $\Hom{R}{M''}{M} \to \Hom{R}{M''}{M''}$ is surjective.)
\end{exercise}

\begin{exercise}
	Let $R$ be a ring that is finitely generated as an abelian group and let $\idealm \subseteq R$ be a maximal ideal.
	Show that $R/\idealm$ is a finite field.
\end{exercise}

\begin{exercise}
	Let $R$ be a ring.
	\begin{enumerate}[label = \alph*)]
		\item Let $I \subseteq R$ be an ideal that is maximal with the property of not being finitely generated.
		Show that $I$ is a prime ideal.
		\item Now assume that every prime ideal $\idealp \subseteq R$ is finitely generated.
		Show that $R$ is Noetherian.
	\end{enumerate}
\end{exercise}

\begin{exercise}
	Let $R$ be a domain and let $a, b \in R$.
	Show that $a$ and $b$ generate the same ideal iff there exists a unit $u \in \units{R}$ with $a = ub$.
	Does the statement still hold, if $R$ is allowed to contain nontrivial zerodivisors?
\end{exercise}

\begin{exercise}
	Let $R$ be a ring and let $\idealm \subseteq R$ be a maximal ideal.
	Show that there exists a (unique) $R$-algebra map $R_{\idealm} \to \complete{R}{\idealm}$.
\end{exercise}

\begin{exercise}
	Let $X$ be a topological space.
	Assume that $X$ is nonempty, quasi-compact and $T_0$.
	Show that there exists a closed point $x \in X$.
\end{exercise}

\begin{exercise}
	Let $R$ be a ring.
	Show that $\Spec{R}$ is a quasi-compact $T_0$-space.
\end{exercise}
\end{document}