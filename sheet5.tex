\newcommand{\lecturenumber}{5}
\input{./intro/intro.tex}

\begin{document}
	\maketitle

\begin{defn}
	Let $X$ be a locally ringed space and let $f \in \calO_X(X)$. We define
	\[ X_f \coloneqq \set[\big]{x \in X}{f_x \in \units{\calO_{X, x}}}. \]
	Note that $X_f \subseteq X$ is an open subset.
\end{defn}

\begin{defn}
	Let $X$ be a scheme and let $x \in X$. Define $\tangent{x}$ (the \emph{tangent space} at $x$) to be the $\residue{x}$-vector space given by
	\[ \tangent{x} \coloneqq \dual{(\idealm_{X,x} \ovr \idealm_{X,x}^2)}. \]
\end{defn}

\begin{defn}
	Let $X, Y \colon \cat{Rings} \to \cat{Set}$ be functors and $f \colon Y \to X$ a map of functors.
	Let $U \subseteq X$ be a subfunctor.
	Define a subfunctor $f^{-1}(U) \subseteq Y$ by setting
	\[ f^{-1}(U)(R) \coloneqq \set[\big]{y \in Y(R)}{f(y) \in U(R)} \]
	for all rings $R$. 
\end{defn}

\begin{defn}
	Let $A$ be a ring.
	Define a functor $\Spec{A} \colon \cat{Rings} \to \cat{Set}$ by setting
	\[ \Spec{A}(R) \coloneqq \Hom{}{A}{R}. \]
	Note that this is just the functor corepresented by $A$.
	
	If moreover $I \subseteq A$ is an ideal, define a subfunctor $\Spec{A}_I \subseteq \Spec{A}$ by setting
	\[ \Spec{A}_I(R) \coloneqq \set[\big]{\varphi \colon A \rightarrow R}{\varphi(I)R = R}. \] 
\end{defn}

\begin{defn}
	Let $X \colon \cat{Rings} \to \cat{Set}$ be a functor.
	\begin{enumerate}
		\item $X$ is called \emph{local} if for every ring $R$ and every family of elements $f_1, \dots, f_n \in R$ generating the unit ideal in $R$, the canonical map
		\[ X(R) \to \set*{(x_i)_i \in \prod_i X(\adjoin{R}{f_i^{-1}})}{\text{$x_i, x_j$ have same image in $X(\adjoin{R}{(f_i f_j)^{-1}})$}} \] 
		is bijective.
		\item A subfunctor $U \subseteq X$ is called \emph{open} if for all rings $A$ and all maps $f \colon \Spec{A} \to X$, there is an ideal $I \subseteq A$ with $f^{-1}(U) = \Spec{A}_I$.
		\item Let $(U_i)_i$ be a family of open subfunctors of $X$. We say that $(U_i)_i$ is an \emph{open cover} of $X$ if we have
		\[ X(K) = \bigcup_i U_i(K) \]
		for all fields $K$.
	\end{enumerate}
\end{defn}

\problemsstart

\begin{exercise}
	Let $X$ be a scheme.
	Show that $X$ is affine if $X$ has at most $2$ points.
	Give an example of a non affine scheme with $3$ points.
\end{exercise}

\begin{exercise}
	Is $X \coloneqq \coprod_{\NN}\Spec{\CC}$ a scheme?
	Is it affine?
	Does $X$ admit an open immersion into an affine scheme?
\end{exercise}

\begin{exercise}[pathological example]
	Let $X$ be a non-empty scheme.
	Is it true, that $X$ always has a closed point?
\end{exercise}

\begin{exercise}
	Let $X$ be a quasi-compact scheme with precisely one closed point.
	Is it true that $X$ is isomorphic to the spectrum of a local ring?
	What about if $X$ is not assumed to be quasi-compact?
\end{exercise}

\begin{exercise}
	Let $X$ be a scheme.
	Let $A \coloneqq \calO_X(X)$ and let $f \in A$.
	\begin{enumerate}[label=\alph*)]
		\item Show that there is a canonical map $\adjoin{A}{f^{-1}} \to \calO_X(X_f)$.
		\item Suppose that $X$ is quasi-compact.
		Show that the above map is injective.
		\item Now suppose that $X$ can be covered by finitely many open affine subsets $U_1, \dots, U_n$ such that $U_i \cap U_j$ is quasi-compact for all $i, j$.
		Show that the above map is an isomorphism.
	\end{enumerate}
\end{exercise}

\begin{exercise}
	Let $X$ be a scheme.
	Let $f_1, \dots, f_n \in \calO_X(X)$ generating the unit ideal such that $X_{f_i}$ is affine for all $i$.
	Show that $X$ is affine.
	
	(Hint: Use the previous exercise.)
\end{exercise}

\begin{exercise}
	Let $X$ be an integral scheme with generic point $\eta \in X$.
	\begin{enumerate}[label=\alph*)]
		\item Show that $K \coloneqq \calO_{X, \eta}$ is a field.
		\item Let $U \subseteq X$ be non-empty affine open. Show that the canonical map $\calO_X(U) \to K$ identifies $K$ with the quotient field of $\calO_X(U)$.
		\item Show that the statement in b) is in general false if we do not assume $U$ to be affine.
	\end{enumerate}
\end{exercise}

\begin{exercise}
	Let $k$ be a field.
	Let $X$ be a scheme over $k$.
	Show that we have a canonical bijection
	\[ \Hom{k}{\Spec{\adjoin{k}{x}\ovr (x^2)}}{X} \cong \set[\Big]{(x, v)}{x \in X, \residue{x} = k, v \in \tangent{x}}. \]
\end{exercise}

\begin{exercise}
	Let $X \colon \cat{Rings} \to \cat{Set}$ be a functor.
	Show that the following are equivalent:
	\begin{enumerate}
		\item $X$ is isomorphic to the functor of points of some scheme.
		\item $X$ is local and has an open cover $(U_i)_i$ such that every $U_i$ is isomorphic to $\Spec{A}$ for some ring $A$.
	\end{enumerate}
\end{exercise}

\end{document}