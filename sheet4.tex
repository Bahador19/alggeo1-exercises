\newcommand{\lecturenumber}{4}
\input{./intro/intro.tex}

\begin{document}
	\maketitle

\begin{defn}
	Let $X$ be a topological space and $\calF$ be a sheaf (of abelian groups) on $X$. 
	\[ \supp{\calF} \coloneqq \set*{x \in X}{\calF_x \neq 0} \]	
	is called the \emph{support} of $\calF$.
\end{defn}

\begin{defn}
	Let $X$ be a topological space and suppose we are given a field $\kappa_x$ for every point $x \in X$. Define a sheaf of rings $\calR_{(\kappa_x)_x}$ on $X$ by setting
	\[ \calR_{(\kappa_x)_x}(U) \coloneqq \set*{s \colon U \to \coprod_{x\in U} \kappa_x}{s(x) \in \kappa_x \text{ for all $x \in U$}} \]
	for every open subset $U \subseteq X$ (and by using the obvious restriction maps).
\end{defn}

\begin{defn}
	A \emph{topological space with functions} consists of
	\begin{enumerate}
		\item a topological space $X$,
		\item for every point $x \in X$ a field $\kappa_x$ and
		\item a subsheaf of rings $\calO_X \subseteq \calR_{(\kappa_x)_x}$,
	\end{enumerate}
	such that for every point $x \in X$, the ring $\calO_{X, x}$ is local with maximal ideal
	\[ \idealm_{X, x} \coloneqq \set[\big]{\overline{s} \in \calO_{X,x}}{s(x) = 0 \in \kappa_x} \]
	and the canonical map
	\[ \calO_{X,x} \ovr \idealm_{X, x} \to \kappa_x \]
	given by evaluation at the point $x$ is an isomorphism.
	
	Let $X, Y$ be two topological spaces with functions.
	A morphism $f \colon X \to Y$ consists of
	\begin{enumerate}
		\item a map of topological spaces $f \colon X \to Y$ and
		\item for every point $x \in X$, a map of fields $f_x \colon \kappa_{f(x)} \to \kappa_x$,
	\end{enumerate}
	such that for any open subset $U \subseteq Y$ and any function $s \in \calO_Y(U)$, the map
	\[ f^{-1}(U) \to \prod_{x \in f^{-1}(U)} \kappa_x, \qquad x \longmapsto f_x(s(f(x))) \]
	defines an element in $\calO_X(f^{-1}(U))$.
	
	There is an evident category of topological spaces with functions.
\end{defn}

\begin{defn}
	Let $X$ be a locally ringed space.
	We say that $X$ is \emph{reduced}, if for all open subsets $U \subseteq X$ and every section $s \in \calO_X(U)$ such that $s(x) = 0 \in \residue{x}$ for all $x \in U$, we already have $s = 0 \in \calO_X(U)$.
\end{defn}

\problemsstart

\begin{exercise}
	Let $X$ be a topological space.
	Let $i \colon \{ x \} \to X$ be the inclusion of a closed point.
	Show that a sheaf $\calF \in \Sh{\cat{Ab}}{X}$ has support $\{ x \}$ iff we have $\calF \cong i_* A$ for some sheaf $A$ on $\{ x \}$.
	Does this statement still hold if $x$ is not closed?
	
	(A sheaf of the form $i_* A$ for some $A$ is called a \emph{skyscraper sheaf}.)
\end{exercise}

\begin{exercise}
	Let $X$ be a topological space.
	Are the following presheaves sheaves?
	\begin{enumerate}[label = \alph*)]
		\item $\calF_1 \colon U \mapsto  \varprojlim_i \calF_i (U)$, where $(\calF_i)_i$ is an inverse system of sheaves.
		\item $\calF_2 \colon U \mapsto  \varinjlim_i \calF_i (U)$, where $(\calF_i)_i$ is a direct system of sheaves.
		\item $\calF_3 \colon U \mapsto  \Hom{U}{\calF|_U}{\calG|_U}$, where $\calF, \calG$ are sheaves.
		\item $\calF_4 \colon U \mapsto  \reduce{\calR(U)}$, where $\calR$ is a sheaf of rings and $\reduce{R}$ denotes the reduction of a ring $R$, i.e.\ the quotient of $R$ by its nilradical.
	\end{enumerate}
\end{exercise}

\begin{exercise}
	Find an example of a topological space $X$ and presheaves $\calF_n \in \Psh{\cat{Ab}}{X}$ for $n \in \NN$, such that we have $\sheafi{\calF_n} = 0$ for all $n \in \NN$, but $\sheafi{\calG} \neq 0$, where $\calG = \prod_n \calF_n$.
\end{exercise}

\begin{exercise}
	Let $X$ be a topological space and let $X = \bigcup_i U_i$ be an open cover of $X$.
	Let $\calF \in \Psh{\cat{Set}}{X}$ and $\calG \in \Sh{\cat{Set}}{X}$.
	Also suppose that we are given, for each $i$, a map of presheaves $\varphi_i \colon \restr{\calF}{U_i} \to \restr{\calG}{U_i}$ such that, for each $i, j$, we have $\restr{\varphi_i}{U_i \cap U_j} = \restr{\varphi_j}{U_i \cap U_j}$.
	
	Show that there exists a unique map of presheaves $\varphi \colon \calF \to \calG$ such that we have $\restr{\varphi}{U_i} = \varphi_i$ for all $i$.
\end{exercise}

\begin{exercise}
	Let $X$ be a locally ringed space.
	\begin{enumerate}[label = \alph*)]
		\item Show that the set $\vanish{s} \coloneqq \set[\big]{x \in U}{s(x) = 0 \in \residue{x} }$ is closed in $U$. This is called the \emph{vanishing set} of $s$.
		\item Show that $s \in \calO_X(U)$ is a unit iff $\vanish{s} = \emptyset$.
	\end{enumerate}
\end{exercise}

\begin{exercise}
	Let $X$ be a ringed space.
	Show that $X$ is a locally ringed space iff the following conditions are satisfied:
	\begin{enumerate}
		\item For every point $x \in X$, we have $\calO_{X,x} \neq 0$.
		\item For every open subset $U \subseteq X$ and every section $s \in \calO_X(U)$ there exists an open cover $U = \bigcup_i U_i$ such that for every $i$, one of the sections $\restr{s}{U_i}$ and $1 - \restr{s}{U_i}$ is a unit in $\calO_X(U_i)$.
	\end{enumerate}
\end{exercise}

\begin{exercise}
	Construct an equivalence between the category of topological spaces with functions and the category of reduced locally ringed spaces.
\end{exercise}

\begin{exercise}
	Let $X$ be a locally ringed space. Consider the following statements:
	\begin{enumerate}
		\item $X$ is reduced.
		\item For every $U \subseteq X$ the ring $\calO_X(U)$ is reduced.
		\item For every $x \in X$, the ring $\calO_{X, x}$ is reduced.
	\end{enumerate}
	Show that we have $(1) \Rightarrow (2) \Leftrightarrow (3)$. Also show that the implication $(2), (3) \Rightarrow (1)$ does not hold.
\end{exercise}

\end{document}